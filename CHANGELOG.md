# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.21](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.20...v0.0.21) (2023-01-04)


### Bug Fixes

* fixed crash when resolving symbol references from subpackages ([b41f584](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/b41f584cbf6185b1f3b29b915eb58b8f0a37e7dc))
* return transitive file dependencies ([ea5d12e](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/ea5d12e44a23c6a6cb2d69f3331a24f71f5350f8))

### [0.0.20](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.19...v0.0.20) (2022-12-30)


### Bug Fixes

* handle package references from service method input/output types ([bb845e2](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/bb845e296699570567cfa610a9ffbb49a4607c3a))

### [0.0.19](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.18...v0.0.19) (2022-12-18)


### Features

* added support for 'allExtensionNumbersOfType' requests ([311310e](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/311310efeb678572504cbf586d119464b79b0368))
* added support for 'fileContainingExtension' requests ([50889a0](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/50889a08829a5343399de7dd0ef63015ac73a60c))


### Bug Fixes

* return transitive proto file dependencies on findByFilename ([9ed6bf5](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/9ed6bf5f95bc832ccb54c372a2d1f2c724eb7aa4))
* set proper sourceRoot to fix asset copying ([f3988f2](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/f3988f2e133c67560de6d8bc7ba5a5d048729aab))
* typo in error log ([dd2a2e1](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/dd2a2e11de6c2744af674b453f522cc8cef19cde))

### [0.0.18](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.17...v0.0.18) (2022-12-01)


### Bug Fixes

* add file deps to descriptor which are required by some clients ([5dbeb72](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/5dbeb7237905a57b94f9f70f990af66521072f55))

### [0.0.17](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.16...v0.0.17) (2022-12-01)


### Features

* misc symbol lookup improvements ([38ca8e4](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/38ca8e46218a656e32a1a79b10a1afc825c5cb26))


### Bug Fixes

* add transitive file dependencies to symbol lookup return ([1165baf](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/1165baf9914307df6ed9a626832a0be2351c0a62))
* reverted change to sample proto file ([c084d26](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/c084d26c568a2298e18bae5810776dd218013221))

### [0.0.16](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.15...v0.0.16) (2022-11-29)

### [0.0.15](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.14...v0.0.15) (2022-11-29)

### [0.0.14](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.13...v0.0.14) (2022-11-28)

### [0.0.13](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.12...v0.0.13) (2022-11-28)

### [0.0.12](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.10...v0.0.12) (2022-11-28)


### Features

* move config package to dev dependencies ([2ba82b7](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/2ba82b7bb8f89ef92cac292c3616da95d608732b))

### [0.0.11](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.10...v0.0.11) (2022-11-28)


### Features

* add support for async module registration ([b9ceec7](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/b9ceec79186dbae2fd9704c16c1b0b3a5536648a))

### [0.0.10](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.9...v0.0.10) (2022-08-18)

### [0.0.9](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.8...v0.0.9) (2022-03-09)


### Bug Fixes

* added raw link to gif to work on external sites like npm ([2868e7e](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/2868e7e9b4ff9b9dac72e5442cdc8f0629f2bba7))

### [0.0.8](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.7...v0.0.8) (2022-03-09)


### Features

* added example gif to README ([361839d](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/361839d59e969ac7d69593b75c5e0de4eccd4a9a))

### [0.0.7](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.6...v0.0.7) (2022-02-22)

### [0.0.6](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.5...v0.0.6) (2022-02-22)


### Features

* added function to add proto paths/package to grpc options ([7b42c8d](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/7b42c8db8e5dd3da9e13863d43ca5f2ceea93604))

### [0.0.5](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.4...v0.0.5) (2022-02-19)


### Features

* added repo and issue tracker information ([6c41506](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/6c415069ca93f977ec97697eb6919659eaaac277))
* updated README ([36fa181](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/36fa181f1a4cbb68cee8b8f930cdd5892b2c3722))

### [0.0.4](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.3...v0.0.4) (2022-02-19)


### Bug Fixes

* fixed broken file descriptor handling when server is running in keepCase mode ([cda146d](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/cda146d5db04b54c7ab6891adc0f4fb3fb57cd8c))

### [0.0.3](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.2...v0.0.3) (2022-02-19)


### Bug Fixes

* added package 'main' file ([e1738f3](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/e1738f3a706887007c8ded139a7a838b5a152f10))
* handle case where grpc proto-loader options aren't set ([7909c7d](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/7909c7d0dee6fc9d916f539e37ce42995d78d2a3))

### [0.0.2](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/compare/v0.0.1...v0.0.2) (2022-02-19)


### Features

* add files to export from library ([da8c323](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/da8c323a393fdfad9cc944d911500298ab63c672))

### 0.0.1 (2022-02-19)


### Features

* added support for different keepCase proto-loader options ([7fe175e](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/7fe175e3a9dcdaed2177ef2daab1e7e022a2cf1c))
* added support for imported proto types ([fdd9512](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/fdd9512cab255b43ee655c99112af8f5e7684b60))
* broke proto path and package out into their own constants ([3e9c1fb](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/3e9c1fbc2e6aeb56308e59ca45fc529c425580d1))
* converted sample app to use grpc ([7d311b7](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/7d311b7df38be48009f3b1b39b8c20088251f763))
* implemented 'file_by_filename' reflection request ([a6baede](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/a6baedef6eb5cf1389000f2625b11e5c177de531))
* implemented fileContainingSymbol reflection method ([d34357a](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/d34357a4e1a3fb2e1468cfc669451f106e5e304a))
* implemented listServices reflection method ([c67a158](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/c67a158d7686dc4812d135009cfaebb33122620e))
* moved app code into a 'sample' dir to make room for lib ([f59c5c5](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/f59c5c5351b70a6114a6cc1cb6af6ef2a1904eb5))
* stubbed out reflection module ([c050ca8](https://gitlab.com/jtimmons/nestjs-grpc-reflection-module/commit/c050ca832f0db7f5699023ce6ff8f49bfb1adc8b))
