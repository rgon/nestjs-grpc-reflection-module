import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions } from '@nestjs/microservices';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { GrpcClientOptions } from './grpc-client.options';

async function bootstrap() {
  const configService: ConfigService = new ConfigService();
  const grpcClientOptions: GrpcClientOptions = new GrpcClientOptions(configService);

  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    grpcClientOptions.getGRPCConfig,
  );
  await app.listen();
}
bootstrap();
