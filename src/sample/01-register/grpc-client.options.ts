import { GrpcOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { addReflectionToGrpcConfig } from '../../grpc-reflection';

export const grpcClientOptions: GrpcOptions = addReflectionToGrpcConfig({
  transport: Transport.GRPC,
  options: {
    package: 'sample',
    protoPath: join(__dirname, '../common/proto/sample.proto'),
    loader: {
      oneofs: true,
      includeDirs: [join(__dirname, '../common/proto/vendor/')],
    },
  },
});
