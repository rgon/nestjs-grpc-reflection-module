import { ModuleMetadata, Type } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';

export interface GrpcReflectionOptionsFactory {
  createGrpcReflectionOptions(): Promise<GrpcOptions> | GrpcOptions;
}

export interface GrpcReflectionModuleAsyncOptions extends Pick<ModuleMetadata, 'imports'> {
  useExisting?: Type<GrpcReflectionOptionsFactory>;
  useClass?: Type<GrpcReflectionOptionsFactory>;
  useFactory?: (...args: any[]) => Promise<GrpcOptions> | GrpcOptions;
  inject?: any[];
}
