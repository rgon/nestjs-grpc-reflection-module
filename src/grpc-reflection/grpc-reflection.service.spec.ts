import { Test } from '@nestjs/testing';
import { GRPC_CONFIG_PROVIDER_TOKEN } from './grpc-reflection.constants';
import { GrpcReflectionService } from './grpc-reflection.service';

// use grpc options from sample service for testing
import { grpcClientOptions } from '../sample/01-register/grpc-client.options';
import { FileDescriptorProto } from 'google-protobuf/google/protobuf/descriptor_pb';

describe('GrpcReflectionService', () => {
  let reflectionService: GrpcReflectionService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        GrpcReflectionService,
        {
          provide: GRPC_CONFIG_PROVIDER_TOKEN,
          useValue: grpcClientOptions,
        },
      ],
    }).compile();

    await moduleRef.init();

    reflectionService = await moduleRef.resolve<GrpcReflectionService>(GrpcReflectionService);
  });

  describe('listServices()', () => {
    it('lists all services', () => {
      const { service: services } = reflectionService.listServices('*');
      expect(services).toHaveLength(3);
      expect(services[0].name).toEqual('sample.SampleService');
    });
  });

  describe('fileByFilename()', () => {
    it('finds files with transitive dependencies', () => {
      const descriptors = reflectionService
        .fileByFilename('sample.proto')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(
        new Set(['sample.proto', 'vendor.proto', 'vendor_dependency.proto']),
      );
    });

    it('finds files with fewer transitive dependencies', () => {
      const descriptors = reflectionService
        .fileByFilename('vendor.proto')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(new Set(['vendor.proto', 'vendor_dependency.proto']));
    });

    it('finds files with no transitive dependencies', () => {
      const descriptors = reflectionService
        .fileByFilename('vendor_dependency.proto')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(1);
      expect(descriptors[0].getName()).toEqual('vendor_dependency.proto');
    });

    it('merges files based on package name', () => {
      const descriptors = reflectionService
        .fileByFilename('vendor.proto')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(names).not.toContain('common.proto'); // combined w/ vendor.proto
    });

    it('errors with no file found', () => {
      expect(() => reflectionService.fileByFilename('nonexistent.proto')).toThrow(
        'Proto file not found',
      );
    });
  });

  describe('fileContainingSymbol()', () => {
    it('finds symbols and returns transitive file dependencies', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('sample.HelloRequest')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(
        new Set(['sample.proto', 'vendor.proto', 'vendor_dependency.proto']),
      );
    });

    it('finds imported message types', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('vendor.CommonMessage')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(new Set(['vendor.proto', 'vendor_dependency.proto']));
    });

    it('finds transitively imported message types', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('vendor.dependency.DependentMessage')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      expect(descriptors).toHaveLength(1);
      expect(descriptors[0].getName()).toEqual('vendor_dependency.proto');
    });

    it('finds nested message types', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('sample.HelloRequest.HelloNested')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(
        new Set(['sample.proto', 'vendor.proto', 'vendor_dependency.proto']),
      );
    });

    it('merges files based on package name', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('vendor.CommonMessage')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(names).not.toContain('common.proto');
    });

    it('errors with no symbol found', () => {
      expect(() => reflectionService.fileContainingSymbol('non.existant.symbol')).toThrow(
        'Symbol not found:',
      );
    });

    it('resolves references to method types', () => {
      const descriptors = reflectionService
        .fileContainingSymbol('sample.SampleService.Hello2')
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(
        new Set(['sample.proto', 'vendor.proto', 'vendor_dependency.proto']),
      );
    });
  });

  describe('fileContainingExtension()', () => {
    it('finds extensions and returns transitive file dependencies', () => {
      const descriptors = reflectionService
        .fileContainingExtension('.vendor.CommonMessage', 101)
        .fileDescriptorProto.map(FileDescriptorProto.deserializeBinary);

      const names = descriptors.map((desc) => desc.getName());
      expect(new Set(names)).toEqual(new Set(['vendor.proto', 'vendor_dependency.proto']));
    });

    it('errors with no symbol found', () => {
      expect(() => reflectionService.fileContainingExtension('non.existant.symbol', 0)).toThrow(
        'Extension not found',
      );
    });
  });

  describe('allExtensionNumbersOfType()', () => {
    it('finds extensions and returns transitive file dependencies', () => {
      const response = reflectionService.allExtensionNumbersOfType('.vendor.CommonMessage');

      expect(response.extensionNumber).toHaveLength(1);
      expect(response.extensionNumber[0]).toEqual(101);
    });

    it('errors with no symbol found', () => {
      expect(() => reflectionService.allExtensionNumbersOfType('non.existant.symbol')).toThrow(
        'Extensions not found',
      );
    });
  });
});
