import { GrpcOptions } from '@nestjs/microservices';

import { REFLECTION_PACKAGES, REFLECTION_PROTOS } from './grpc-reflection.constants';

/** Adds gRPC reflection package and proto information to a NestJS GrpcOptions object
 *
 * This is intended to be used to inform NestJS of where to find the reflection proto file
 * and what package to look out for requests on, it does *not* however add the logic for
 * processing incoming requests. This needs to be paired with the {@link GrpcReflectionModule}
 * module which provides the message handlers and reflection implementation
 */
export const addReflectionToGrpcConfig = (config: GrpcOptions): GrpcOptions => {
  const protoPath = Array.isArray(config.options.protoPath)
    ? config.options.protoPath
    : [config.options.protoPath];
  const pkg = Array.isArray(config.options.package)
    ? config.options.package
    : [config.options.package];

  return {
    ...config,
    options: {
      ...config.options,
      protoPath: [...protoPath, ...REFLECTION_PROTOS],
      package: [...pkg, ...REFLECTION_PACKAGES],
    },
  };
};
