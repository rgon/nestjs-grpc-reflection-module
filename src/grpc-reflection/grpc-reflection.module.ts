import { DynamicModule, Module, Provider } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';
import { V1AlphaGrpcReflectionController, V1GrpcReflectionController } from './controllers';
import { GRPC_CONFIG_PROVIDER_TOKEN } from './grpc-reflection.constants';
import { GrpcReflectionService } from './grpc-reflection.service';
import {
  GrpcReflectionModuleAsyncOptions,
  GrpcReflectionOptionsFactory,
} from './interfaces/grpc-reflection-module-options.interface';

/** Adds gRPC Reflection API support to a NestJS gRPC application
 *
 * Note: This module only contains the controllers and business logic for the
 * reflection API, but does not actually load the proto files into the running
 * gRPC server. It needs to be paired with the {@link addReflectionToGrpcConfig}
 * utility function in order to accept incoming messages.
 */
@Module({})
export class GrpcReflectionModule {
  /** Registers a new gRPC reflection module using static configuration options
   *
   * @example
   *   @Module({
   *     imports: [GrpcReflectionModule.register({
   *       transport: Transport.GRPC,
   *       options: {
   *       package: 'sample',
   *       protoPath: join(__dirname, '../common/proto/sample.proto'),
   *         loader: {
   *           includeDirs: [join(__dirname, '../common/proto/vendor/')],
   *         },
   *       },
   *     })]
   *   })
   *   export class AppModule {}
   *
   * @see the {@link GrpcReflectionModule.html#registerAsync | registerAsync}
   * method if your config relies on another NestJS module or other async events
   */
  static register(grpcOptions: GrpcOptions): DynamicModule {
    return {
      module: GrpcReflectionModule,
      controllers: [V1AlphaGrpcReflectionController, V1GrpcReflectionController],
      providers: [
        GrpcReflectionService,
        {
          provide: GRPC_CONFIG_PROVIDER_TOKEN,
          useValue: grpcOptions,
        },
      ],
    };
  }

  /** Registers a new gRPC reflection module using async configuration options
   *
   * @example
   *   @Module({
   *     imports: [GrpcReflectionModule.registerAsync({
   *       inject: [ConfigService],
   *       useFactory: async (config: ConfigService) => ({
   *         transport: Transport.GRPC,
   *         options: {
   *           url: `127.0.0.1:${config.get<number>('port') || 5000}`,
   *           package: 'sample',
   *           protoPath: join(__dirname, '../common/proto/sample.proto'),
   *           loader: {
   *             includeDirs: [join(__dirname, '../common/proto/vendor/')],
   *           },
   *         },
   *       })
   *     })]
   *   })
   *   export class AppModule {}
   *
   * This is commonly used if the gRPC config is pulled from a config file
   *
   * @see the {@link GrpcReflectionModule.html#register | register} method if
   * your config options can be created syncronously
   */
  static registerAsync(options: GrpcReflectionModuleAsyncOptions): DynamicModule {
    return {
      module: GrpcReflectionModule,
      controllers: [V1AlphaGrpcReflectionController, V1GrpcReflectionController],
      imports: options.imports || [],
      providers: this.createAsyncProviders(options),
    };
  }

  private static createAsyncProviders(options: GrpcReflectionModuleAsyncOptions): Provider[] {
    const providers: Provider[] = [GrpcReflectionService];

    if (options.useFactory) {
      providers.push({
        provide: GRPC_CONFIG_PROVIDER_TOKEN,
        useFactory: options.useFactory,
        inject: options.inject || [],
      });
    } else if (options.useExisting) {
      providers.push({
        provide: GRPC_CONFIG_PROVIDER_TOKEN,
        useFactory: async (optionsFactory: GrpcReflectionOptionsFactory) =>
          await optionsFactory.createGrpcReflectionOptions(),
        inject: [options.useExisting],
      });
    } else {
      providers.push({
        provide: GRPC_CONFIG_PROVIDER_TOKEN,
        useFactory: async (optionsFactory: GrpcReflectionOptionsFactory) =>
          await optionsFactory.createGrpcReflectionOptions(),
        inject: [options.useClass],
      });
      providers.push({
        provide: options.useClass,
        useClass: options.useClass,
      });
    }

    return providers;
  }
}
