import {
  FileDescriptorProto,
  FileDescriptorSet,
} from 'google-protobuf/google/protobuf/descriptor_pb';

import * as grpc from '@grpc/grpc-js';
import * as protoLoader from '@grpc/proto-loader';
import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { GrpcOptions } from '@nestjs/microservices';

import { GRPC_CONFIG_PROVIDER_TOKEN } from './grpc-reflection.constants';
import {
  ExtensionNumberResponse,
  FileDescriptorResponse,
  ListServiceResponse,
} from './proto/grpc/reflection/v1/reflection';
import { visit } from './protobuf-visitor';

export class ReflectionError extends Error {
  constructor(readonly statusCode: grpc.status, readonly message: string) {
    super(message);
  }
}

/** Analyzes a gRPC server and exposes methods to reflect on it
 *
 * NOTE: the files returned by this service may not match the handwritten ones 1:1.
 * This is because proto-loader reorients files based on their package definition,
 * combining any that have the same package.
 *
 * For example: if files 'a.proto' and 'b.proto' are both for the same package 'c' then
 * we will always return a reference to a combined 'c.proto' instead of the 2 files.
 */
@Injectable()
export class GrpcReflectionService implements OnModuleInit {
  private readonly logger = new Logger(GrpcReflectionService.name);

  /** The full list of proto files (including imported deps) that the gRPC server includes */
  private fileDescriptorSet = new FileDescriptorSet();

  /** An index of proto files by file name (eg. 'sample.proto') */
  private fileNameIndex: Record<string, FileDescriptorProto> = {};

  /** An index of proto files by type extension relationship
   *
   * extensionIndex[<pkg>.<msg>][<field#>] contains a reference to the file containing an
   * extension for the type "<pkg>.<msg>" and field number "<field#>"
   */
  private extensionIndex: Record<string, Record<number, FileDescriptorProto>> = {};

  /** An index of fully qualified symbol names (eg. 'sample.Message') to the files that contain them */
  private symbolMap: Record<string, FileDescriptorProto> = {};

  constructor(
    @Inject(GRPC_CONFIG_PROVIDER_TOKEN)
    private readonly grpcConfig: GrpcOptions,
  ) {}

  async onModuleInit() {
    const { protoPath, loader } = this.grpcConfig.options;
    const protoFiles = Array.isArray(protoPath) ? protoPath : [protoPath];
    const packageDefinitions = await Promise.all(
      protoFiles.map((file) => protoLoader.load(file, loader)),
    );

    packageDefinitions.forEach((packageDefinition) => {
      Object.values(packageDefinition).forEach(({ fileDescriptorProtos }) => {
        // Add file descriptors to the FileDescriptorSet.
        // We use the Array check here because a ServiceDefinition could have a method named the same thing
        if (Array.isArray(fileDescriptorProtos)) {
          fileDescriptorProtos.forEach((bin) => {
            const proto = FileDescriptorProto.deserializeBinary(bin);
            const isFileInSet = this.fileDescriptorSet
              .getFileList()
              .map((f) => f.getName())
              .includes(proto.getName());
            if (!isFileInSet) {
              this.fileDescriptorSet.addFile(proto);
            }
          });
        }
      });
    });

    this.fileNameIndex = Object.fromEntries(
      this.fileDescriptorSet.getFileList().map((f) => [f.getName(), f]),
    );

    // Pass 1: Index Values
    const index = (fqn: string, file: FileDescriptorProto) => (this.symbolMap[fqn] = file);
    this.fileDescriptorSet.getFileList().forEach((file) =>
      visit(file, {
        field: index,
        oneOf: index,
        message: index,
        service: index,
        method: index,
        enum: index,
        enumValue: index,
        extension: (fqn, file, ext) => {
          index(fqn, file);

          const extendeeName = ext.getExtendee();
          this.extensionIndex[extendeeName] = {
            ...(this.extensionIndex[extendeeName] || {}),
            [ext.getNumber()]: file,
          };
        },
      }),
    );

    // Pass 2: Link References To Values
    const addReference = (ref: string, sourceFile: FileDescriptorProto) => {
      if (!ref) {
        return; // nothing to do
      }

      let referencedFile: FileDescriptorProto | null = null;
      if (ref.startsWith('.')) {
        // absolute reference
        referencedFile = this.symbolMap[ref.replace(/^\./, '')];
      } else {
        referencedFile = this.symbolMap[ref]; // try fqn lookup
        if (!referencedFile) {
          referencedFile = this.symbolMap[`${sourceFile.getPackage()}.${ref}`]; // try relative lookup
        }
      }

      if (!referencedFile) {
        this.logger.warn(`Could not find file associated with reference ${ref}`);
        return;
      }

      if (referencedFile !== sourceFile) {
        sourceFile.addDependency(referencedFile.getName());
      }
    };

    this.fileDescriptorSet.getFileList().forEach((file) =>
      visit(file, {
        field: (_fqn, file, field) => addReference(field.getTypeName(), file),
        extension: (_fqn, file, ext) => addReference(ext.getTypeName(), file),
        method: (_fqn, file, method) => {
          addReference(method.getInputType(), file);
          addReference(method.getOutputType(), file);
        },
      }),
    );
  }

  /** List the full names of registered gRPC services
   *
   * note: the spec is unclear as to what the 'listServices' param can be; most
   * clients seem to only pass '*' but unsure if this should behave like a
   * filter. Until we know how this should behave with different inputs this
   * just always returns *all* services.
   *
   * @returns full-qualified service names (eg. 'sample.SampleService')
   */
  listServices(listServices: string): ListServiceResponse {
    this.logger.debug(`listServices called with filter ${listServices}`);
    const services = this.fileDescriptorSet
      .getFileList()
      .map((file) =>
        file.getServiceList().map((service) => `${file.getPackage()}.${service.getName()}`),
      )
      .flat();

    this.logger.debug(`listServices found services: ${services.join(', ')}`);
    return { service: services.map((service) => ({ name: service })) };
  }

  /** Find the proto file(s) that declares the given fully-qualified symbol name
   *
   * @param symbol fully-qualified name of the symbol to lookup
   * (e.g. package.service[.method] or package.type)
   *
   * @returns descriptors of the file which contains this symbol and its imports
   */
  fileContainingSymbol(symbol: string): FileDescriptorResponse {
    this.logger.debug(`fileContainingSymbol called for symbol ${symbol}`);
    const file = this.symbolMap[symbol];

    if (!file) {
      this.logger.error(`fileContainingSymbol failed to find symbol ${symbol}`);
      throw new ReflectionError(grpc.status.NOT_FOUND, `Symbol not found: ${symbol}`);
    }

    const deps = this.getFileDependencies(file);
    this.logger.debug(
      `fileContainingSymbol found files: ${[file, ...deps].map((f) => f.getName())}`,
    );

    return {
      fileDescriptorProto: [file, ...deps].map((proto) => proto.serializeBinary()),
    };
  }

  /** Find a proto file by the file name
   *
   * @returns descriptors of the file which contains this symbol and its imports
   */
  fileByFilename(filename: string): FileDescriptorResponse {
    this.logger.debug(`fileByFilename called with filename ${filename}`);
    const file = this.fileNameIndex[filename];

    if (!file) {
      throw new ReflectionError(grpc.status.NOT_FOUND, `Proto file not found: ${filename}`);
    }

    const deps = this.getFileDependencies(file);
    this.logger.debug(`fileByFilename found files: ${[file, ...deps].map((f) => f.getName())}`);

    return {
      fileDescriptorProto: [file, ...deps].map((f) => f.serializeBinary()),
    };
  }

  /** Find a proto file containing an extension to a message type
   *
   * @returns descriptors of the file which contains this symbol and its imports
   */
  fileContainingExtension(symbol: string, field: number): FileDescriptorResponse {
    const extensionsByFieldNumber = this.extensionIndex[symbol] || {};
    const file = extensionsByFieldNumber[field];

    if (!file) {
      throw new ReflectionError(
        grpc.status.NOT_FOUND,
        `Extension not found for symbol ${symbol} at field ${field}`,
      );
    }

    const deps = this.getFileDependencies(file);
    this.logger.debug(
      `fileContainingExtension found files: ${[file, ...deps].map((f) => f.getName())}`,
    );

    return {
      fileDescriptorProto: [file, ...deps].map((f) => f.serializeBinary()),
    };
  }

  allExtensionNumbersOfType(symbol: string): ExtensionNumberResponse {
    if (!(symbol in this.extensionIndex)) {
      throw new ReflectionError(grpc.status.NOT_FOUND, `Extensions not found for symbol ${symbol}`);
    }

    const fieldNumbers = Object.keys(this.extensionIndex[symbol]).map((key) => Number(key));

    return {
      baseTypeName: symbol,
      extensionNumber: fieldNumbers,
    };
  }

  private getFileDependencies(
    file: FileDescriptorProto,
    visited: Set<FileDescriptorProto> = new Set(),
  ): FileDescriptorProto[] {
    const newVisited = visited.add(file);

    const directDeps = file.getDependencyList().map((dep) => this.fileNameIndex[dep]);
    const transitiveDeps = directDeps
      .filter((dep) => !newVisited.has(dep))
      .map((dep) => this.getFileDependencies(dep, newVisited))
      .flat();

    const allDeps = [...directDeps, ...transitiveDeps];

    return [...new Set(allDeps)];
  }
}
