import { join } from 'path';

import { protobufPackage as v1alphaProtobufPackage } from './proto/grpc/reflection/v1alpha/reflection';
import { protobufPackage as v1ProtobufPackage } from './proto/grpc/reflection/v1/reflection';

/** @deprecated no longer valid because this library supports multiple reflection API versions
 *
 * use the 'addReflectionToGrpcConfig' utility function or the 'REFLECTION_PROTOS' constant instead.
 * This points to the outdated 'v1alpha' proto definition and will be removed in a future version
 */
export const REFLECTION_PROTO = join(__dirname, './proto/grpc/reflection/v1alpha/reflection.proto');

/** @deprecated no longer valid because this library supports multiple reflection API versions
 *
 * use the 'addReflectionToGrpcConfig' utility function or the 'REFLECTION_PACKAGES' constant instead.
 * This points to the outdated 'v1alpha' proto definition and will be removed in a future version
 */
export const REFLECTION_PACKAGE = v1alphaProtobufPackage;

export const REFLECTION_PACKAGES = [v1alphaProtobufPackage, v1ProtobufPackage];

export const REFLECTION_PROTOS = [
  join(__dirname, './proto/grpc/reflection/v1alpha/reflection.proto'),
  join(__dirname, './proto/grpc/reflection/v1/reflection.proto'),
];

export const GRPC_CONFIG_PROVIDER_TOKEN = 'GRPC_CONFIG_OPTIONS';
